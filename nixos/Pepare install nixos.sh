#!/usr/bin/env bash

# Choose the disk to install NixOS on
echo "Available disks:"
lsblk -d -n -o NAME,SIZE | awk '{print NR ". " $1 " " $2}'
read -p "Enter the disk number to install NixOS on: " disk_number

# Get the disk name from the selected disk number
disk_name=$(lsblk -d -n -o NAME | awk -v disk_number="$disk_number" 'NR == disk_number')

# Check if the disk is valid
if [[ -z $disk_name ]]; then
  echo "Invalid disk number. Exiting..."
  exit 1
fi

# Create the EFI partition
parted /dev/$disk_name mklabel gpt
parted /dev/$disk_name mkpart primary fat32 1MiB 513MiB
parted /dev/$disk_name set 1 boot on

# Create the BTRFS partition
parted /dev/$disk_name mkpart primary btrfs 513MiB 100%

# Format the partitions
mkfs.fat -F32 /dev/${disk_name}1
mkfs.btrfs -L nixos /dev/${disk_name}2

# Mount the partitions
mount /dev/${disk_name}2 /mnt
mkdir -p /mnt/boot/efi
mount /dev/${disk_name}1 /mnt/boot/efi

# Create subvolumes
btrfs subvolume create /mnt/root
btrfs subvolume create /mnt/home
btrfs subvolume create /mnt/nix

# Mount the subvolumes
umount /mnt
mount -o compress=zstd,subvol=root /dev/${disk_name}2 /mnt
mkdir /mnt/{home,nix}
mount -o compress=zstd,subvol=home /dev/${disk_name}2 /mnt/home
mount -o compress=zstd,subvol=nix /dev/${disk_name}2 /mnt/nix

echo "Disk setup completed!"
