# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
 imports =
    [ # Include the results of the hardware scan.
    ./hardware-configuration.nix

      <home-manager/nixos> 
  ];

    nixpkgs.config.allowUnfree = true;

  # Use the GRUB EFI boot loader.
 boot.loader = {
  efi = {
    canTouchEfiVariables = true;
    efiSysMountPoint = "/boot/"; # ← use the same mount point here.
  };
  grub = {
    efiSupport = true;
    device = "nodev";
  };
};
 boot.loader.systemd-boot.configurationLimit = 7;
 networking.hostName = "nixos"; # Define your hostname.
  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
   networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

zramSwap = {
    enable = true;
    algorithm = "zstd";
  };

 #autoupdater
 system.autoUpgrade.enable = true;
system.autoUpgrade.allowReboot = false;

#autocleanup and optimize
#nix.settings.auto-optimise-store = true;
nix.gc = {
  automatic = true;
  dates = "weekly";
  options = "--delete-older-than 7d";
};

  # Set your time zone.
   time.timeZone = "Europe/Amsterdam";

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
   i18n.defaultLocale = "nl_NL.UTF-8";
   console = {
     font = "Lat2-Terminus16";
     keyMap = "us";
   #useXkbConfig = true; # use xkbOptions in tty.
   };

  # Enable the X11 windowing system.
  services.xserver ={
  enable=true;
    layout = "us";
 xkbVariant="intl";
 };


  # Enable Desktop Environments.
  services.xserver.displayManager.lightdm.enable = true;
  services.xserver.displayManager.lightdm.greeters.enso.enable = true;

services.udev.packages = with pkgs; [ gnome.gnome-settings-daemon ];
services.xserver.desktopManager.xfce.enable = true;
sound.mediaKeys.enable = true;

services.xserver.windowManager.i3.enable =true;
services.xserver.windowManager.i3.package = pkgs.i3-gaps;
services.xserver.windowManager.i3.extraPackages = with pkgs; [rofi i3altlayout nitrogen i3status i3lock];

services.xserver.windowManager.bspwm.enable =true;
  # Configure keymap in X11
 #  services.xserver.layout = "us";
  # services.xserver.xkbOptions = {
   #  "eurosign:e";
    # "caps:escape"; # map caps to escape.
  # };

  # Enable CUPS to print documents.
  # services.printing.enable = true;

#pipewire sound
hardware.pulseaudio.enable = false;
services.pipewire = {
  enable = true;
  alsa.enable = true;
  alsa.support32Bit = true;
  pulse.enable = true;
};

#Bluetooth
hardware.bluetooth.enable = true;

# Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
   users.users.simon = {
     isNormalUser = true;
     extraGroups = [ "wheel" "libvirtd" ]; # Enable ‘sudo’ for the user.

     packages = with pkgs; [
       google-chrome
       libreoffice-fresh
       obs-studio
       gnomeExtensions.appindicator
      # spotify
       gettext
       neofetch
       pkgs.cool-retro-term    
       pulseaudio-ctl
       picom 
       kitty
       pcem
       hunspell
       hunspellDicts.nl_nl
       appimage-run
       blueberry
       spotify-tui
       pavucontrol
       spotifyd
       playerctl
       feh
       polybar
       lxappearance
       lightdm-enso-os-greeter    
       variety

     ];
   };
  #Fonts and theming

  fonts.fonts = with pkgs; [
     noto-fonts-cjk
     noto-fonts-emoji
     corefonts
     vistafonts
     font-awesome
     nerdfonts
     wpgtk    

   ];
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
     vim_configurable  # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
     wget
     virt-manager
     git  
     pkgs.xfce.xfce4-whiskermenu-plugin    
     pkgs.xfce.xfce4-pulseaudio-plugin    

 ];      

  # Some programs need SUID wrappers, can be configured further or are
  programs.dconf.enable = true;
  virtualisation.libvirtd.enable = true;

  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  #homemanager

  home-manager.users.simon = { pkgs, ... }: {
home.packages = [ pkgs.ncspot pkgs.krita pkgs.ayu-theme-gtk     
   
 ];
  programs.bash.enable = true;
  programs.bash.initExtra = "neofetch"; 
gtk = {
  enable = true;
  font.name = "Victor Mono SemiBold 12";
  theme = {
    name = "SolArc-Dark";
    package = pkgs.solarc-gtk-theme;
  };
};

};
      


  # steam anabler

  programs.steam.enable = true;

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;
  # This value determines the NixOS release from which the default



  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?

}
